# yum

function yum_create_repository()
{
    local name=;
    local baseurl=;
    local root=;
    local force=false;
    
    OPTIND=1
    while getopts "r:n:u:d:f" opt; do
	case $opt in
	    f) force=true;;
	    r) root="$OPTARG";;
	    n) name="$OPTARG";;
	    u) baseurl="$OPTARG";;
	    d) 
		if [ ! -d "$OPTARG" ]; then
		    log -e "Base directory $OPTARG does not exist";
		    return 1;
		fi;
		baseurl="file://$OPTARG/";;
	esac;
    done;
    
    shift $((OPTIND-1));
    
    if [ -z "$root" ]; then
	log -e "Root not defined";
	return 1;
    fi;
    if [ ! -d "$root" ]; then
	log -e "Root directory $root does not exist";
	return 1;
    fi;
    
    local repo="$root/etc/yum.repos.d";
    mkdir -p "$repo";
    
    local repofile;
    if [ -z "$name" ]; then
	repo=$(mktemp -p "$repo" repoXXXXXX);
	name=$(basename "$repo");
	repofile="${repo}.repo";
    else 
	repo="$repo/$name";
	repofile="${repo}.repo";
	if [ -f "$repofile" ] && ! $force; then
	    log -e "Repository file ${repofile} already exists";
	    return 1;
	fi;
    fi;
    
    cat > "${repofile}" <<EOF
[${name}]
name=${name}
baseurl=${baseurl}
gpgcheck=0
repo_gpgcheck=0
enabled=1
keepcache=0
EOF
    log -i "Added repository $baseurl : ${repofile}";
    echo "${repofile}";
    return 0;
}

#
# Install pacakges into a filesystem root. It is assumed that the repositories have already been setup
# appropriately.
# 
# Usage:
#  zypper_install_packages -r <root> [-f] <package>...
# Return:
#  0 on success
#  1 on error
# 
yum_install_packages()
{
    local root=;
    local force=false;

    OPTIND=1
    while getopts "r:f" opt; do
	case "$opt" in
	    f) force=true;;
	    r) root="$OPTARG";;
	esac;
    done;
    shift $((OPTIND-1));
    log -i -- "--installroot='$root' install -y '$@'";
    yum  --installroot="$root" install -y "$@" |& while read LINE; do
	log -i -- $LINE;
    done;
    if [ ${PIPESTATUS[0]} -ne 0 ]; then
	return 1;
    else
	return 0;
    fi;
}
