#
# this package simulates shell-globals that can be used
# across functions; these should be used for global
# that need to be set, e.g. exit traps.
#

# this is the folder where all the global variables will be declare

g_encode_key()
{
    echo "$1" | base64 -w 0;
}

g_decode_key()
{
    echo "$1" | base64 -d 
}

#
# set a global variable value. If the value is omitted
# then the variables is set to the input from stdin.
#
# Usage:
#  set_global -A <map> <key> [<value>]
#  set_global -a <array> <index> [<value>]
#  set_global <key> [<value>]
# Return
#  0 if successfully set
#  1 on any error
#
g_set()
{
    local outdir="${GLOBAL_VALUES}";
    if [ $# -ne 2 ]; then
	OPTIND=1;
	while getopts "A:a:" opt; do
	    case $opt in
		A) outdir="${GLOBAL_MAPS}/${OPTARG}";;
		a) outdir="${GLOBAL_ARRAYS}/${OPTARG}";;
	    esac;
	done;
	shift $((OPTIND-1));
    fi;

    if [ -z "$1" ]; then
	log -e "Invalid set_global command; missing key and/or value";
	exit 1;
    fi;

    mkdir -p "$outdir";
    if [ ! -d "$outdir" ]; then
	log -e "Failed to set the global variable $outdir";
	exit 1;
    fi;
    local key=$(g_encode_key ${1});
    if [ -n "$2" ]; then
	echo "$2" > "${outdir}/$key";
    else
	cat > "${outdir}/$key";
    fi;
}

# Usage:
#  get_global -A <map> <key>
#  get_global -a <array> <index>
#  get_global <key>
# Return:
#  0 on success and the value on stdout
#  1 if not found
#
g_get() {
    local outdir="${GLOBAL_VALUES}";
    if [ $# -ne 1 ]; then
	OPTIND=1;
	while getopts "A:a:" opt; do
	    case $opt in
		A) outdir="${GLOBAL_MAPS}/${OPTARG}";;
		a) outdir="${GLOBAL_ARRAYS}/${OPTARG}";;
	    esac;
	done;
	shift $((OPTIND-1));
    fi;

    if [ $# -ne 1 ]; then
	log -e "Invalid get_global command; missing key";
	exit 1;
    fi;
    local key=$(g_encode_key ${1});

    if [ -f "${outdir}/${key}" ]; then
	cat "${outdir}/${key}";
    else
	return 1;
    fi;
}

# Usage:
#  unset_global -A <map> <key>
#  unset_global -a <array> <index>
#  unget_global <key>
# Return:
#  0 
#
g_unset() {
    local outdir="${GLOBAL_VALUES}";
    if [ $# -ne 1 ]; then
	OPTIND=1;
	while getopts "A:a:" opt; do
	    case $opt in
		A) outdir="${GLOBAL_MAPS}/${OPTARG}";;
		a) outdir="${GLOBAL_ARRAYS}/${OPTARG}";;
	    esac;
	done;
	shift $((OPTIND-1));
    fi;

    if [ -n "$1" ]; then
	local key=$(g_encode_key ${1});
	outdir="${outdir}/${key}";
    fi;

    rm -rf ${outdir};
    return 0;
}


# Usage:
#  declare_global -A <map>
#  declare_global -a <array>
#  declare_global <key>
# Return:
#  0 
#
g_declare() {
    local outdir="${GLOBAL_VALUES}";
    if [ $# -eq 1 ]; then
	local key=$(g_encode_key ${1});
        touch "${GLOBAL_VALUES}/${key}";
	return 0;
    fi;

    OPTIND=1;
    while getopts "A:a:" opt; do
	case $opt in
	    A) outdir="${GLOBAL_MAPS}/${OPTARG}";;
	    a) outdir="${GLOBAL_ARRAYS}/${OPTARG}";;
	esac;
    done;
    shift $((OPTIND-1));
    
    mkdir -p ${outdir};
    return 0;
}

# 
# Get the keys in global map
# Usage:
#  g_keys <map>
# Return:
#  0 and keys of the map on stdout
g_keys()
{
    local outdir="${GLOBAL_MAPS}/${1}";

    if [ -d "$outdir" ]; then
	for f in $(ls "$outdir"); do
	    g_decode_key $f;
	done;
    fi;
    return 0;
}

globals_destroy()
{
    
    log -d "deleting all globals $GLOBALS";
    rm -rf "$GLOBALS";
}

globals_init()
{
    if [ -z "$GLOBALS" ]; then
	export GLOBALS=$(mktemp -d "globals.XXXXXXXXXX");
    fi;
    
    if [ $? -ne 0 ]; then
	log -e "Failed to create globals";
	return 1;
    fi;
    
    log -d "Using global variables in $GLOBALS";
    
    export GLOBAL_MAPS="$GLOBALS/maps";
    export GLOBAL_ARRAYS="$GLOBALS/arrays";
    export GLOBAL_VALUES="$GLOBALS/values";

    return 0;
}

