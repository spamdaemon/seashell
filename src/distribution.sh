#!/bin/bash

#
# Locate all files that some kind of version information embedded.
# Usage:
#  find_file [-d <dir>] [-v <version-string>] [-r] <basename>
# Return
#  0 and a list of files in version order, or reversed if -r is specified
#  1 if there are no files
# 
find_files()
{
    local name=;
    local version=;
    local reverse_it=false;
    local dir="./";

    OPTIND=1;
    while getopts "d:rv:" opt; do
	case "$opt" in
	    d) dir="$(readlink -e "$OPTARG")";;
	    v) version="$OPTARG";;
	    r) reverse_it=true;;
	esac;
    done;
    shift $((OPTIND-1));
    name="$1";

# locate all files
    local names=;
    local prefix="$name";
    
    if [ -f "${dir}/${prefix}" ] || [ -d "${dir}/${prefix}" ]; then
	names="${dir}/${prefix}";
    fi;
    
    for v in ${version//-/ }; do
	prefix="${prefix}-${v}";
	log -d "Searching for ${dir}/$prefix";
	if [ -f "${dir}/${prefix}" ] || [ -d "${dir}/${prefix}" ]; then
	    if [ -z "$names" ]; then
		names="${dir}/${prefix}";
	    else
		names="${names}\n${dir}/${prefix}";
	    fi;
	fi;
    done;

    log -d "Found file $names";
    if [ -z "$names" ]; then
	return 1;
    fi;

    if $reverse_it; then
	names="$(reverse_list $names)";
    fi;
    echo -e "$names";
    exit 0;
}

#
# List a entries in an overlay spec.
#
# Usage:
#  get <key> <ov>
# Return:
#  0 and the entries defined by key on stdout
read_overlay_spec()
{
    local -a LINE;
    local version=;
    local reverse_it=;
    local combine=false;
    local specs=;

    OPTIND=1;
    while getopts "d:rv:c" opt; do
	case "$opt" in
	    v) version="-v $OPTARG";;
	    r) reverse_it="-r";;
	    c) combine=true;;
	esac;
    done;
    shift $((OPTIND-1));
    local key="$1";
    local overlay="$2";

    specs="$(find_files $version $reverse_it -d "${overlay}" spec)";
    local result=;
    for spec in $specs; do
	local tmp_result=$(
	    egrep "^${key}\s*(:|\s)" "$spec" | sed -re "s/${key}\s*(:|\s)\s*(.*)\$/\2/g"| while read -a LINE; do
		for e in "${LINE[@]}"; do
		    echo "$e";
		done;
	    done);
	if [ -z "$tmp_result" ]; then
	    continue;
	fi;

	if [ -z "$result" ]; then
	    result="$tmp_result";
	elif [ -n "$tmp_result" ]; then
	    result="${result}\n${tmp_result}";
	fi;
	if ! $combine; then
	    break;
	fi;
    done;
    if [ -z "${result}" ]; then
	return 0;
    fi;
    echo -e "${result}" | sort -u;
    return 0;
}

#
# List the packages in the overlay
# Usage:
#  get_packages <ov> <version>
# Return:
#  0 and the list of packages on stdout
#
get_packages()
{
    local version=;
    if [ -n "$2" ]; then
	version="-v $2";
    fi;
    read_overlay_spec $version -r "package" "${1}";
    return 0;
}

#
# List the repositories in the overlay
# Usage:
#  get_repositories <ov>
# Return:
#  0 and the list of packages on stdout
#
get_repositories()
{
    local version=;
    if [ -n "$2" ];  then
	version="-v $2";
    fi;
    read_overlay_spec $version -r "repo" "${1}";
}

#
# List the rpms in the overlay
# Usage:
#  get_rpmss <ov>
# Return:
#  0 and the list of packages on stdout
#
get_rpms()
{
    local version=;
    if [ -n "$2" ]; then
	version="-v $2";
    fi;
    local rpms="$(find_files -r $version -d "${1}" rpms | head -1)";
    
    if [ -n "$rpms" ]; then
	find "$rpms" -name \*.rpm;
    fi;

    read_overlay_spec $version -r "rpm" "${1}";
    return 0;
}

#
# List the rpms in the overlay
# Usage:
#  get_repositories <ov>
# Return:
#  0 and the list of packages on stdout
#
get_dependencies()
{
    local version=;
    if [ -n "$2" ]; then
	version="-v $2";
    fi;
    read_overlay_spec $version -c "dep" "${1}";
    return 0;
}

# Topologically sort overlays
# Usage:
#  topsort_overlays <overlay>...
# Return
# 0 the overlays and all dependencies sorted on stdout
# 1 on error
#
function sort_overlays()
{
    local -A overlay_deps;
    local overlays;
    local queue;
    local next_queue;
    local current;
    local unresolved_deps=
    local -A new_queue
    local search_path='.'
    local version=;

    OPTIND=1;
    while getopts "p:" opt; do
	case "$opt" in
	    p) search_path="$OPTARG";;
	    v) version="-v $OPTARG";;
	esac;
    done;
    shift $((OPTIND-1));
    queue="$@";

    while [ -n "$queue" ]; do
	progress=false;
	new_queue=()
	
	for current in $queue; do

# the only time overlay_deps is empty is when the overlay 
# is encountered for the first time
	    if [ -z "${overlay_deps[$current]}" ]; then
		overlay_deps["$current"]=$(list_overlay_dependencies $version -p "$search_path" $current);
		if [ $? -ne 0 ]; then
		    log -e "Failed to determine overlays for $current";
		    return 1;
		fi;
		progress=true;
	    fi;

# update the dependency list to see if there any newly resolved dependencies
	    unresolved_deps=
	    for dep in ${overlay_deps["$current"]}; do
		if [ "$dep" !=  "${overlay_deps[$dep]}" ]; then
		    new_queue["$dep"]="$dep";
		    new_queue["$current"]="$current";
		    unresolved_deps="$dep $unresolved_deps";
		fi;
	    done;

# if there are no more unresolved dependencies for the current item in the
# queue, then we emit the current item
	    if [ -z "$unresolved_deps" ] && [ "$current" !=  "${overlay_deps[$current]}" ]; then
		overlay_deps["$current"]="$current";
		overlays="$overlays $current";
	    fi;

# if the current item has already been emitted, then the dependencies will be set to itself
	    if [ "$current" ==  "${overlay_deps[$current]}" ]; then
		progress=true;
	    fi;

	done;
	queue="${new_queue[@]}";
	if ! $progress; then
	    log -e "No progress made: there seems to be a cycle : queue was $queue";
	    return 1;
	fi;
    done;

    echo "$overlays";
    return 0;
}


#
# Find the overlay location.
# Usage
#  get_dependency <dependency>
# Return
#  0 and the folder containing the overlay definition
#  1 if there is no such overlay
#
function find_overlay()
{
    local search_path
    OPTIND=1;
    while getopts "p:" opt; do
	case "$opt" in
	    p) search_path="$OPTARG";;
	esac;
    done;
    shift $((OPTIND-1));

    if [ -z "$1" ]; then
	log -e "No overlay specified";
	return 1;
    fi;
    if [ -z "$search_path" ]; then
	log -w "No search path specified";
	search_path='.';
    fi;

    for p in $(split_path "$search_path"); do
	if [ -d "${p}/${1}" ]; then
	    echo "${p}/${1}";
	    return 0;
	fi;
    done;

    log -e "No such overlay $1 found in $search_path";
    return 1;
}

#
# Get the dependencies of an overlay. Dependencies are specified
# in the file deps.
# Usage
#  list_overlay_dependencies <overlay>
# Return
#  0 and a sorted list of overlays on stdout
#  1 if there is no such overlay
#
function list_overlay_dependencies()
{
    local search_path=;
    local version=;

    OPTIND=1;
    while getopts "v:p:" opt; do
	case "$opt" in
	    p) search_path="$OPTARG";;
	    v) version="$OPTARG";;
	esac;
    done;
    shift $((OPTIND-1));

    local loc=$(find_overlay -p "$search_path" "$@");
    if [ -z "$loc" ]; then
	return 1;
    fi;

    local entry;
    for d in $(get_dependencies "${loc}" $version); do
	entry=$(sed -re 's/^(\+)(.*)$/\1 \2/g' <<<"${d}");
	read opt value <<<"${entry}";
	if [ "$opt" != "+" ]; then 
	    echo ${d};
	elif find_overlay "${value}" >/dev/null 2>/dev/null; then
	    echo "${value}";
	else
	    log -i "Optional overlay $value not found";
	fi;
    done;

    return 0;
}


is_overlay_out_of_date()
{
    local rootfs="/";
    local buildfs="/tmp";
    local search_path='.'
    local version;

    OPTIND=1
    while getopts "r:b:p:v:" opt; do
	case "$opt" in
	    r) rootfs="$(readlink -m $OPTARG)";;
	    b) buildfs="$(readlink -m $OPTARG)";;
	    p) search_path="$OPTARG";;
	    v) version="-v $OPTARG";;
	esac;
    done;
    shift $((OPTIND-1))


    local overlay item dep;

    if [ ! -f "${buildfs}/${1}/.installed" ]; then
	return 0;
    fi;

    overlay=$(find_overlay -p "$search_path" $1);
    if [ $? -ne 0 ]; then
	log -e "No such overlay $1 to install";
	return 0;
    fi;
    
    
    
    # check if any of the dependencies have been installed more recently that this one
    for dep in $(list_overlay_dependencies $version -p "$search_path" $1); do
	local that_overlay="${buildfs}/${dep}/.installed";
	if compare_mod_time "${buildfs}/${1}/.installed" "$that_overlay" > /dev/null ; then
	    return 0;
	fi;
    done;

    # check if the contents of the overlay are more recent than the installed
    for item in $(find "$overlay" -type f); do
	if compare_mod_time "${buildfs}/${1}/.installed" "$item" > /dev/null ; then
	    return 0;
	fi;
	if [ -L "$item" ]; then
	    item=$(readlink -e $item); 
	    if compare_mod_time "${buildfs}/${1}/.installed" "$item" > /dev/null ; then
		return 0;
	    fi;
	fi;
    done;
    
    return 1;
}


# 
# Install an individual overlay. All dependencies have already been installed!
# Usage:
#  install_overlay <overlay>
# Return
#  0 on success
#  1 on failure
#  2 if the overlay did not have to be remade
# 
install_overlay()
{
    local rootfs="/";
    local buildfs="/tmp";
    local search_path=.;
    local version=;

    OPTIND=1
    while getopts "r:b:p:v:" opt; do
	case "$opt" in
	    r) rootfs="$(readlink -m $OPTARG)";;
	    b) buildfs="$(readlink -m $OPTARG)";;
	    p) search_path="$OPTARG";;
	    v) version="-v $OPTARG";;
	esac;
    done;
    shift $((OPTIND-1))

   local overlay status;
   overlay=$(find_overlay -p "$search_path" $1);
   if [ $? -ne 0 ]; then
       log -e "No such overlay $1 to install";
       return 1;
   fi;
   
   
    if ! is_overlay_out_of_date $version -p "$search_path" -b "$buildfs"  $1; then
	log -i "Overlay $1 already installed";
	return 2;
    fi;
    
    status=0;
    local installer="$(find_files $version -r -d "${overlay}" install | head -1)";
    log -i "Installing overlay $1 ($overlay) : ${installer}";
    
    if [ -z "${installer}" ]; then
	log -w "No installer for $1 found";
    elif [ -x "${installer}" ]; then
	(cd "${overlay}" && "${installer}") |& log -ir;
	status=${PIPESTATUS[0]};
    else
	status=1;
	log -e "Found installer '${installer}', but it is not executable : ";
    fi;
   
    if [ $status -ne 0 ]; then
	log -e "Failed to install overlay $@";
	return 1;
    fi;
    
    ftouch "${buildfs}/${1}/.installed";
    return 0;
}

#
# Install the RPMs of the specified overlays
#
# Usage
#  install_rpm <overlay>...
# Return
#  0 on success 
#  1 on failure
install_overlay_packages()
{
    local rootfs="/";
    local build;
    local search_path='.';
    local version=
    OPTIND=1
    while getopts "p:r:b:v:" opt; do
	case "$opt" in
	    r) rootfs="$OPTARG";;
	    b) build="$OPTARG";;
	    p) search_path="$OPTARG";;
	    v) version="$OPTARG";;
	esac;
    done;
    shift $((OPTIND-1))

    local ov overlay rpms;
    # gather the rpms for the overlay
    rm -f "${build}/repos.tmp";
    rm -f "${build}/packages.tmp";
    touch "${build}/repos.tmp" "${build}/packages.tmp"
    for ov in "$@"; do
	overlay=$(find_overlay -p "$search_path" $ov);
	if [ $? -ne 0 ]; then
	    return 1;
	fi;
	get_packages "${overlay}" $version >> "${build}/packages.tmp";
	get_repositories "${overlay}" $version >> "${build}/repos.tmp";
    done;

    sort -u "${build}/packages.tmp" > "${build}/packages.new";
    rm -f "${build}/packages.tmp";
    sort -u "${build}/repos.tmp" > "${build}/repos.new";
    rm -f "${build}/repos.tmp";
    
    if [ -f "${build}/packages" ] && diff "${build}/packages.new" "${build}/packages"; then
	log -i "Packages already installed";
	return 2;
    fi;


    # build the command line to install the packages
    local cmdline="install_packages  -r '$rootfs'";
    while read REPO; do
	cmdline="$cmdline -i '$REPO'";
    done <"${build}/repos.new";
    while read PACKAGE; do 
	cmdline="$cmdline '$PACKAGE'";
    done < "${build}/packages.new";

    log -i "Command line : $cmdline";
    eval $cmdline;
    if [ $? -ne 0 ]; then
	log -e "Failed to install packages $@";
	return 1;
    fi;
    mv -f "${build}/packages.new" "${build}/packages";
    mv -f "${build}/repos.new" "${build}/repos";
    return 0;
}

#
# Install the RPMs of the specified overlays
#
# Usage
#  install_rpm <overlay>...
# Return
#  0 on success 
#  1 on failure
install_overlay_rpm()
{
    local rootfs="/";
    local build;
    local search_path='.';
    local version=;

    OPTIND=1
    while getopts "v:r:b:p:" opt; do
	case "$opt" in
	    r) rootfs="$OPTARG";;
	    b) build="$OPTARG";;
	    p) search_path="$OPTARG";;
	    v) version="$OPTARG";;
	esac;
    done;
    shift $((OPTIND-1));

    local ov overlay rpms;
    # gather the rpms for the overlay
    rm -f "${build}/rpms.tmp";
    touch "${build}/rpms.tmp";
    for ov in "$@"; do
	overlay=$(find_overlay -p "$search_path" $ov);
	if [ $? -ne 0 ]; then
	    return 1;
	fi;
	get_rpms "${overlay}" $version >> "${build}/rpms.tmp";
    done;

    sort -u "${build}/rpms.tmp" > "${build}/rpms.new";
    rm -f "${build}/rpms.tmp";
    
    if [ -f "${build}/rpms" ] && diff "${build}/rpms.new" "${build}/rpms"; then
	log -i "Rpms already installed";
	return 2;
    fi;

    local cmdline=rpm_install;
    while read RPM; do
	cmdline="$cmdline '$RPM'";
    done < "${build}/rpms.new";
    
    if [ "$cmdline" != "rpm_install" ]; then
	eval $cmdline;
    fi;

    mv -f "${build}/rpms.new" "${build}/rpms";
    return 0;
}

#
# Create an operating system image ready for booting on a separate host.
# This is primarily used for pxe bootable images.
#  OS_ROOT       - the root file system into which to install
#  OS_BUILD_ROOT - the root of the build area
#  OS_TMP        - a scratch area which is deleted after installation
#
# Usage
#  assemble_os <overlay>
# Return
#  0 if the overlay was out-of-date and installed
#  1 if the overlay failed to install
#  2 if the overlay did not need to be installed again
#
function create_os_offline()
{
    local rootfs;
    local buildfs="/tmp";
    local make_clean=false;
    local search_path=
    local version=

    usage()
    {
	message "$0 [-h] [-c] [-r <root>] [-b <build>] [-v <version>] [-p <search-path>]";
	message " Install OS and overlay packages.";

	message " -h    this message";
	message " -c    make a clean install";
	message " -r    the root of the filesystem where the overlay will be installed";
	message " -b    the build area that will be used to store state information for the build";
	message " -p    a directory to search for overlays; repeat multiple times (default: .)";
	message " -v    the distribution id of the form: <distro>-<major>-<minor>-<patch>-<arch>";
	message "       supported value for distro are: opensuse, rhel, fedora, ubuntu";

	if [ -z "$1" ]; then
	    exit 1;
	else
	    exit $1;
	fi;
    }

    OPTIND=1
    while getopts "hcr:b:p:v:" opt; do
	case "$opt" in
	    h) usage 0;;
	    r) rootfs="$(readlink -m "$OPTARG")";;
	    b) buildfs="$(readlink -m "$OPTARG")";;
	    c) make_clean=true;;
	    p) search_path="$search_path:$OPTARG";;
	    v) version="-v ${OPTARG}";;
	esac;
    done;

    shift $((OPTIND-1))

    local uid=$(id -u);
    if [ $uid -ne 0 ]; then
	message "OS installation requires root priviledges";
	exit 1;
    fi;


    if [ -z "$search_path" ]; then
	search_path='.';
    fi;

    if [ -z "$rootfs" ]; then
	log -e "No root specified";
	return 1;
    fi;

    if [ "/" == "$rootfs" ]; then
	# for now, don't allow / as the rootfs otherwise we'll end with a broken host most likely
	log -e "System root must not be specified";
	return 1;
    fi;

    export OS_ROOT="$rootfs";
    export OS_BUILD_ROOT="$buildfs";

    if $make_clean; then
	rm -rf "${OS_BUILD_ROOT}";
	if [ "$rootfs" == "/" ]; then
	    log -w "Not cleaning the root directory $OS_ROOT";
	else
	    rm -rf "$rootfs";
	fi;
    fi;

    mkdir -p "${OS_ROOT}";
    
    if [ ! -d "${OS_ROOT}" ]; then
	log -e "Failed to create root directory $OS_ROOT";
	return 1;
    fi;
    
    mkdir -p "${OS_BUILD_ROOT}";
    if  [ ! -d "${OS_BUILD_ROOT}" ]; then
	log -e "Failed to create build directory ${OS_BUILD_ROOT}";
	return 1;
    fi;
    
# get the dependency closure of the overlays
    local sorted_overlays overlay ov status scratchfs;

    sorted_overlays=$(sort_overlays -p "$search_path" "$@");
    if [ $? -eq 1 ]; then
	log -e "Failed to determine overlay-sort order : $@";
	return 1;
    fi;

    # find all repos and packages and isntall them at the same time
    install_overlay_packages $version -p "$search_path" -r "${rootfs:-/}" -b "$buildfs" $sorted_overlays;
    
    # find all rpms of the overlays and install them at the same time
    install_overlay_rpm $version -p "$search_path" -r "${rootfs:-/}" -b "$buildfs" $sorted_overlays;

    scratchfs=$(mktemp -d);
    if [ $? -ne 0 ]; then
	log -e "Failed to create a scratch file area";
	return 1;
    fi;

# run the start script on ALL overlays
    local cleanup=
    for ov in $sorted_overlays; do
	mkdir -p "${scratchfs}/${ov}";
	cleanup="$cleanup ${ov}";
	overlay=$(find_overlay -p "$search_path" $ov);
	if [ -f "${overlay}/start.sh" ]; then
	    export OS_TMP="${scratchfs}/${ov}";
	    (cd ${overlay} && bash "./start.sh");
	fi;
    done;

    local post_install=
# run the install scripts for each overlay
# for those that have been updated, remember to run the post-install script
    for ov in $sorted_overlays; do
	export OS_TMP="${scratchfs}/${ov}";
	mkdir -p "${buildfs}/${ov}";
	export OS_BUILD="${buildfs}/${ov}";
	export OV="${ov}";
	install_overlay $version -p "$search_path" -r "$rootfs" -b "$buildfs" $ov;
	case $? in
	    0) 
		post_install="$post_install $ov";
		true;;
	    1) 
		log -e "Failed to install overlay ${ov}"; 
		return 1;;
	    2) 
		false;;
	esac;
    done;

# once all overlays have been added, run the post install scripts on each overlay
    for ov in $post_install; do
	overlay=$(find_overlay -p "$search_path" $ov);
	if [ -f "${overlay}/postinstall.sh" ]; then
	    export OS_TMP="${scratchfs}/${ov}";
	    (cd ${overlay} && bash "./postinstall.sh");
	fi;
    done;

# run the stop script in ALL overlays
    for ov in $cleanup; do
	overlay=$(find_overlay -p "$search_path" $ov);
	if [ -f "${overlay}/stop.sh" ]; then
	    export OS_TMP="${scratchfs}/${ov}";
	    (cd ${overlay} && bash "./stop.sh");
	fi;
    done;

    rm -rf "${scratchfs}";

    return 0;
}

