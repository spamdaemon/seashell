#!/bin/bash

# 
# Exit the current shell invocation.
# Usage
#  abort [-m <message>] [<code>]
#
abort() {
    local message=
    OPTIND=1
    while getopts "m:" opt; do
	case $opt in 
	    m) message="$OPTARG";;
	    ?) message="abort: Unsupported option";;
	esac;
    done;
    shift $(($OPTIND - 1));

    if [ -n "$message" ]; then
	log -e "$message";
    fi;

    exit ${1:-1};
}

#
# Abort the current shell invocation unless
# the specified error code is 0
# Usage
#  abort_on_error <code> <message>...
#
abort_on_error()
{
    if [ $# -eq 0 ]; then
	log -e "abort_on_error: Missing argument";
	abort;
	return 1;
    fi;


    local status=$1;
    if [ "$status" != "0" ]; then
	shift;
	abort -m "$@";
	return 1;
    fi;

    return 0;
}

