
# 
# List the processes that are actually running.
#
# Usage
#  list_processes <pid>...
# Return
#  0 and the list of processes with that id (in no particular order)
#
function list_processes()
{
    if [ $# -ne 0 ]; then
	ps --pid "$*" -o pid=;
    fi;
    return 0;
}


# List the processes that make up the process tree
# of each listed process.
# Usage:
#  list_process_tree <pid>...
# Return:
#  0 and a list of processes (in no particular order)
function list_process_tree()
{
    if [ $# -ne 0 ]; then
	echo $@;
	local procs=$(list_child_processes "$@");
	echo $(list_process_tree ${procs});
    fi;
    return 0;
}

#
# List the child processes of the specified processes.
#
# Usage:
#  list_child_processes <pid>...
# Return:
#  0 and a list of processes on stdout
#
function list_child_processes()
{
    if [ $# -ne 0 ]; then
	ps --ppid "$*" -o pid=;
    fi;
    return 0;
}


# 
# Kill the trees of the specified processes by successively
# sending a list of signals to the processes until none
# remain or no signals are left to be sent.
# If no signals are specified, then SIGKILL is sent.
#
# Usage:
#  kill_process_tree [-s <signals>] <pid...>
# Return:
#  0 if the all processes were determined 
#  1 if not all processes of the tree could be killed and a list of unkilled processes on stdout
#
function kill_process_tree()
{
    local signals=;
    OPTIND=1;
    while getopts "s:" opt; do
	case $opt in 
	    s) signals+=" $OPTARG";;
	esac;
    done;
    shift $((OPTIND-1));

    if [ -z "$signals" ]; then
	signals=9;
    fi;


# get the original set of processes to kill
# we don't refresh this list, but only reduce it
# by sending more and more signals
    local procs=$(list_process_tree $@);
    for sig in $signals; do
	if [ -z "$procs" ]; then
# we killed all processes, so we can exit the loop
	    break;
	fi;
	kill -$sig $procs;
	procs=$(list_processes $procs);
    done;

    if [ -n "$procs" ]; then
	echo $procs;
	return 1;
    else
	return 0;
    fi;
}

#
# Start a background process.
#
# Usage:
#  start_process [-o <stdout-capturefile>] [-e <stderr-capturefile>] [-p <pidfile>] COMMAND
# Return:
#  0 and prints the actual pidfile on stdout
#
function start_process()
{
    local stdout=;
    local stderr=;
    local pidfile=;

    OPTIND=1;
    while getopts "o:e:p:" opt; do
	case $opt in
	    o) stdout="$OPTARG";;
	    e) stderr="$OPTARG";;
	    p) pidfile="$OPTARG";;
	esac;
    done;

    shift $((OPTIND-1));

    if [ "$stdout" == "$stderr" ] && [ -n "$stdout" ]; then
	stderr='2>&1';
	stdout=">'$stdout'";
    else 
	if [ -n "$stdout" ]; then
	    stdout=">'$stdout'";
	fi;
	if [ -n "$stderr" ]; then
	    stderr="2>'$stderr'";
	fi;
    fi;

    if [ -n "$pidfile" ] && [ -f "$pidfile" ]; then
	log -e "Pidfile $pidfile already exists";
	return 1;
    fi;
    
    eval "$@ $stderr $stdout" &
    PID=$!;

    if [ -n "$pidfile" ]; then
	ps --pid "$PID" -o pid= -o lstart= >"$pidfile";
	echo "$pidfile";
    fi;
    return 0;
}


#
# Stop a process started with start_process
# 
# Usage
#  stop_process [-s <signals>] <pidfile>
#
function stop_process()
{
    local signals=9;
    OPTIND=1;
    while getopts "s:" opt; do
	case $opt in
	    s) signals+=" $OPTARG";;
	esac;
    done;
    shift $((OPTIND-1));

    local retval=0;
    for p in "$@"; do
	local procs=$(status_process -s "$p");
	procs=$(kill_process_tree -s "$signals" $procs);
	if [ -z "$procs" ]; then
	    rm -f "$p";
	fi;
	if [ -f "$p" ]; then
	    log -e "Failed to kill all processes for $p or delete the pid file";
	    retval=1;
	fi;
    done;
    return $retval;
}

#
# Get the process status. If a pidfile does not exist
# then - - <pidfile> is printed in the long version, or nothing
# in the short version
# Usage
#  status_process [-s] [-l] <pidfile>
# Return
#  0 and the status of each process
function status_process()
{
    local short=0
    OPTIND=1;
    while getopts "sl" opt; do
	case $opt in
	    s) short=1;;
	    l) short=0;;
	esac;
    done;
    shift $((OPTIND-1));

    local status;
    for f in "$@"; do
	if [ -f "$f" ]; then
	    read PID LTIME < "$f";
	    status=$(ps --pid $PID -o lstart=);
	    if [ $short -eq 0 ]; then
		if [ "$status" == "$LTIME" ]; then
		    echo "$PID RUNNING $f";
		else
		    echo "$PID STOPPED $f";
		fi;
	    elif [ "$status" == "$LTIME" ]; then
		echo "$PID";
	    fi;
	else 
	    log -w "No such pidfile $f";
	    if [ $short -eq 0 ]; then
		echo "- - $f";
	    fi;
	fi;
    done;
    return 0;
}
