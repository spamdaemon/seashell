

#
# Convert a string to a handle.
function to_handle()
{
    if [ $# -eq 0 ]; then
	base64 -w 0;
    else
	base64 -w 0 <<<"$1";
    fi;
}

function from_handle()
{
    if [ $# -eq 0 ]; then
	base64 -d;
    else
	base64 -d <<<"$1";
    fi;
}
