#!/bin/bash

#
# Continuously evaluate an expression until it is true.
# While the expression is false, this function rotates
# set of symbols at current cursor position with a given
# period.
# The environment variable ROTATE_CURSOR_ITERATION is available
# and will be increased by 1 with each iteration
#
# Usage:
#  rotate_cursor [-p <floating_period in seconds>] [-s "<space separated symbols>"] <condition...> 
#
function rotate_cursor()
{

    local -a symbols=( '-' '\' '|' '/' );
    local period=0.125;
    local haveFinal=0;

    OPTIND=1;
    while getopts "s:p:c:" opt; do
	case $opt in
	    s) symbols=( $OPTARG );;
	    p) period="$OPTARG";;
	esac;
    done;

    shift $(($OPTIND-1))

    if [ $# -eq 0 ]; then
	log -e "No condition specified";
	return 1;
    fi;


    ROTATE_CURSOR_ITERATION=0;
    local nsymbols=${#symbols[@]};
    local current;
    local j;

# save the current cursor position
    
    local saveCursor="$(tput sc)$(tput civis)";
    local clearEOL="$(tput rc)$(tput el)";
    local restoreCursor="$(tput rc)$(tput cvvis)";

    add_exit_trap $0 echo -n ${restoreCursor} &>/dev/null;

    echo -n "${saveCursor}";
    while true; do
	echo -n "${clearEOL}";
		
	"$@" &> /dev/null;
	if [ $? -eq 0 ]; then
	    break;
	fi;

	(( j=(ROTATE_CURSOR_ITERATION)% $nsymbols ));

	current=${symbols[$j]};

	echo -n "$current";

	# get the next symbol
	(( ROTATE_CURSOR_ITERATION=ROTATE_CURSOR_ITERATION+1 ));

	# with gnu sleep we can sleep fractional seconds!
	sleep "$period";
    done;

# the cursor position will always be at that prior to the loop
   
    unset ROTATE_CURSOR_ITERATION;

    remove_exit_trap $0 1;

    return 0;
}

