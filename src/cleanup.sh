
#g_declare -A EXIT_TRAPS;

#
# Log the current exit traps.
#
log_exit_traps()
{
    log -d "Registered exit traps:";
     for key in $(g_keys EXIT_TRAPS); do
	 log -d "Registered exit trap: ${key}";
    done;
    return 0;
}

#
# Add a new exit trap
#
# Usage
#  add_exit_trap <key> <expression>
# Return
#  0 and the previously bound expression on stdout
#  1 if no key or no expression are specified
# 
add_exit_trap()
{
    local name="$1";

    if [ -z "$name" ]; then
	log -e "Missing exit trap name";
	return 1;
    fi;

    shift;

    local fn="$@";

    if [ -z "$fn" ]; then
	log -e "Missing trap function";
	return 1;
    fi;

    local old=$(g_get -A EXIT_TRAPS "$name");
    g_set -A EXIT_TRAPS "$name" "$fn";
    log -d "Added an exit trap $name";
    if [ -n "$old" ]; then
	echo $old;
    fi;
    return 0;
}

#
# Remove an exit trap an optionally execute it.
# If the specified flag is 1, then the trap, if it
# was set, is executed.
# Usage
#  remove_exit_trap <key> [<flag>]
# Return
#  0 if the trap was set
#  1 if the trap was not set 
remove_exit_trap()
{
    local name="$1";
    local t=$(g_get -A EXIT_TRAPS "$name");
    g_unset -A EXIT_TRAPS "$name";
    if [ "$2" == "1" ] && [ -n "$t" ]; then
	$t;
    fi;
    log -d "Removed exit trap $name";
    [ -n "$t" ];
    return $?;
}

execute_exit_traps()
{
    log -d "Execute exit traps";
    for key in $(g_keys EXIT_TRAPS); do
	log -d "Executing exit trap ${key}";
	eval $(g_get -A EXIT_TRAPS "$key");
    done;

# this must be done last!!!
    globals_destroy;
}

cleanup_init() {
# FIXME: we should join the previous exit trap with the new one
    trap execute_exit_traps EXIT ;
    return 0;
}

