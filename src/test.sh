#!/bin/bash

#
# Test the result of a function against an expected value
# Usage:
#  expect [-m <message>] [-v <expected_value>] [-s <expected_status>] command ...
# Return:
#  0 on success
#  1 on error
#
function expect() {
    
    local message="";
    local expected_value="";
    local expected_status=0;
    local expected_substring_value="";
    local actual_status;
    local actual_value;
    
    OPTIND=1
    while getopts "m:e:v:s:-" opt; do
	case $opt in
	    m) message=$OPTARG;;
	    e) expected_value=$OPTARG;;
	    v) expected_substring_value=$OPTARG;;
	    s) expected_status=$OPTARG;;
	    -) break;;
	    ?) message "Invalid option for expect $opt"; 
		exit 2;;
	esac;
    done;
    
    shift $(($OPTIND-1));
    
    if [ -n "$message" ]; then
	message -n "$message...";
    else
	message -n "$@...";
    fi;

    actual_value=$("$@");
    actual_status=$?;

    if [ $actual_status -ne $expected_status ]; then
	echo -e "FAIL:\n  expected status: $expected_status\n     found: $actual_status";
	return 1;
    fi;
    if [ -n "$expected_value" ]; then
	if [ "$actual_value" != "$expected_value" ]; then
	    echo -e "FAIL:\n  expected: $expected_value\n     found: $actual_value";
	    return 1;
	fi;
    elif [ -n "$expected_substring_value" ]; then
	if ! fgrep "$expected_suibstring_value" > /dev/null <<<${actual_value}; then
	    echo -e "FAIL:\n  expected: $expected_value\n     found: $actual_value";
	fi;
    fi;

    echo "OK";
    return 0;
}
