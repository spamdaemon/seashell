#!/bin/bash

apply_file_attributes()
{
    local permissions;
    local group user;

    OPTIND=1;
    while getopts "P:g:u:" opt; do
	case $opt in
	    P) permissions="$OPTARG";;
	    g) group="$OPTARG";;
	    u) user="$OPTARG";;
	    -) break;;
	esac;
    done;
    shift $((OPTIND-1));

    if [ -n "$permissions" ]; then
	chmod "$permissions" "$@";
    fi;
    
    if [ -n "$group" ] || [ -n "$user" ]; then
	chmod "${user}:${group}" "$@";
    fi;
    
    return 0;
}


#
# Get the absolute path of a file name
# Usage
#  abs_path <files>
# Return:
#  the absolute path for each file on stdout and a result of 0
#  1 on error
#
function abs_path()
{
# readlink will print the path to stdout
    for f in "$@"; do
	readlink -m "$f" 2>/dev/null;
	if [ $? -ne 0 ]; then
	    log -w "Failed to determine abs_path of $f";
	    return 1;
	fi;
    done;
    return 0;
}

#
# Get the absolute directory path of the specified pathname.
# Usage
#  abs_dir <path>
# Return
#  the absolute path of the directory part of the specified path
#
function abs_dir()
{
    local abs=$(abs_path "$1");
    if [ -z "$abs" ]; then
	log -w "Absolute path of $1 is empty";
	return 1;
    fi;
    local dir=$(dirname "$abs");
    echo $dir;
    return 0;
}

#
# Make directories for the specified file.
# 
# Usage:
#  make_dirs <file>...
# Return:
#  0 if directories for all files were created
#  1 if only some directories were created
function make_dirs()
{
    local RETVAL=0;
    local dir;
    for f in "$@"; do
	dir=$(abs_dir "$f");
	mkdir -p "$dir";
	if [ ! -d "$dir" ]; then
	    log -e "Failed to create destination directory $dir";
	    RETVAL=1;
	fi;
    done;
    
    return $RETVAL;
}



# Get the last modification time of a file.
# Usage:
#  mod_time <files>
# Return:
#  the modification time of each file and 0 if each file existed
#  1 if some file did not exist
#
function mod_time() 
{
    TZ=UTC stat -c '%y' "$@";
    abort_on_error $? "Failed to obtain status for a file";
    return 0;
}

#
# Determine if a file's modification is older than some other files.
# Usage
#  is_older_than <reference-file> <file>...
# Return
#  0 if the file is older than every other file
#  1 if the file is not older than at least one other file
function compare_mod_time()
{
    local cond="lt";

    OPTIND=1;
    if getopts "c:" opt; then
	case $opt in
	    c) cond=$OPTARG;
	esac;
    fi;
    shift $(($OPTIND-1))
    
    local refTime=$(mod_time "$1");
    shift;

    local fTime;
    result=0;

    for f in "$@"; do
	fTime=$(mod_time "$f");
	local outcome=-1;
	case $cond in
	    eq) [[ "$refTime" == "$fTime" ]]; 
		outcome=$?;;
	    lt) [[ "$refTime" < "$fTime" ]]; 
		outcome=$?;;
	    gt) [[ "$refTime" > "$fTime" ]]; 
		outcome=$?;;
	esac;
	
#	log -i "Tested $refTime $cond $fTime : $outcome";
	if [ $outcome -ne 0 ]; then
	    result=1;
	    echo "$f";
	fi;
    done;
    return $result;
}


#
# Copy a single file from one place to another. The directories
# for the necessary target will be created.
# Usage:
#  copy_file <file> <newfile>
# Return:
#  0 if the file was copied
#  1 if the file did not exist
#  2 if the file could not be copied
#
function copy_file()
{
    local permissions;
    local user group;

    OPTIND=1;
    while getopts "u:g:P:" opt; do
	case $opt in
	    P) permissions="$OPTARG";;
	    u) user="$OPTARG";;
	    g) group="$OPTARG";;
	    -) break;;
	esac;
    done;
    shift $((OPTIND-1));

    local source="$1";
    local dest="$2";

    if [ -z "$dest" ]; then
	dest="$source";
	source=;
    fi;

    if [ -z "$dest" ]; then
	log -e "Missing destination file";
	return 1;
    fi;

    if [ -n "$source" ] && [ ! -f "$source" ]; then 
	log -e "Source file $source not found";
	return 1;
    fi;

    if [ -f "$dest" ]; then
	local oUser oGroup oPerm;
	read oUser oGroup oPerm <<<$(stat -c '%u %g %a' "$dest");
	if [ -z "$user" ]; then
	    user="${oUser}";
	fi;
	if [ -z "$group" ]; then
	    group="${oGroup}";
	fi;
	if [ -z "$permissions" ]; then
	    permissions="$oPerm";
	fi;
    else
	make_dirs "$dest";
	if ! make_dirs "$dest"; then
	    return 2;
	fi;
    fi;

    if [ -z "$source" ]; then
	cat > "$dest";
    else
	cp -f "$source" "$dest";
    fi;    

    apply_file_attributes -u "$user" -g "$group" -p "$permissions"  "$dest";

    return 0;
}


# 
# Copy multiple files to a target directory. If the target directory
# does not exist, then it is created.
# Usage:
#  copy_files <files...> <target-dir>
# Return:
#  0 if all files were successfully copied
#  1 if the target directory could not be created
function copy_files()
{
    local permissions;
    local user group;

    OPTIND=1;
    while getopts "u:g:P:" opt; do
	case $opt in
	    g) group="$OPTARG";;
	    u) user="$OPTARG";;
	    P) permissions="$OPTARG";;
	    -) break;;
	esac;
    done;
    shift $((OPTIND-1));

    local dir="$($#)";
   
    local success=0;
    mkdir -p "$dir";
    if [ ! -d "$dir" ]; then
	return 2;
    fi;

    for f in "${@: 1:-1}"; do
	copy_file -u "$user" -g "$group" -p "$permissions" "$f" "${dir}/$(basename $f)"
    done;
    return 0;
}


#
# Filter a single filter into a target file. Multiple filters
# can be provided. Each filter reads from stdin and produces 
# output on stdout. 
# If no output is specified, then the input file itself is modified
# If no input and output file are specified, then the filter works on stdin and writes to stdout.
# Usage:
#  filter_filter -f <filter> [<input> [<output>]]
# Return:
#  the true if filtering was successful, false otherwise
#
function filter_file()
{

# build the filter chain
    local permissions local;
    local user group;
    local filter_chain;
    OPTIND=1;
    while getopts "u:g:P:f:" opt; do
	case $opt in
	    g) group="$OPTARG";;
	    u) user="$OPTARG";;
	    P) permissions="$OPTARG";;
	    f) 
		if [ -z "$filter_chain" ]; then 
		    filter_chain="$OPTARG"; 
		else 
		    filter_chain="$filter_chain | $OPTARG"; 
		fi;;
	esac;
    done;
    shift $(($OPTIND-1));

    if [ -z "$filter_chain" ]; then
	filter_chain='cat';
    fi;

    
    local fin="$1";
    if [ -z "$fin" ]; then
	# just execute the filter chain here
	eval "$filter_chain";
	return $?;
    fi;

    local fout="$2";
    if [ -z "$fout" ]; then
	fout="$fin";
    fi;

# create the destination directory and a temporary file
# so we can deal with a filter_file x x;
    fout=$(abs_path "$fout");
    local dir=$(dirname "$fout");
    mkdir -p "$dir";
    [ -d "$dir" ];
    abort_on_error $? "Failed to create directory $dir";
    local tmp;
    tmp=$(mktemp -p "$dir");
    [ -f "$tmp" ];
    abort_on_error $? "Failed to create directory $dir";

    
    eval "$filter_chain <$fin >$tmp";
    if [ $? -ne 0 ]; then
	rm -f "$tmp";
	return 1;
    fi;
    mv -f "$tmp" "$fout";

    apply_file_attributes -u "$user" -g "$group" -p "$permissions"  "$fout";
    return 0;
}

#
# Apply multiple filters to each listed file and place the file in the specified directory.
# If an output directory is not specified, then the file is patched in place.
# Usage:
#  filter_files [-f <filter>] [-o <output-directory>] <file>...
function filter_files()
{
# build the filter chain
    local permissions local ;
     local user group;
   local filter_chain=
    local dir=
    OPTIND=1;
    while getopts "u:g:P:f:o:" opt; do
	case $opt in
	    g) group="$OPTARG";;
	    u) user="$OPTARG";;
	    P) permissions="$OPTARG";;
	    f) 
		if [ -z "$filter_chain" ]; then 
		    filter_chain="$OPTARG";
		else
		    filter_chain="$filter_chain | $OPTARG";
		fi;;
	    o) fout="$OPTARG";;
	esac;
    done;

    shift $(($OPTIND-1));

    if [ -z "$filter_chain" ]; then
	filter_chain=cat;
    fi;

    for f in $(first_args "$@"); do
	local fn=$(basename $f);
	if [ -z "$dir" ]; then
	    filter_file -u "$user" -g "$group" -P "$permissions" -f "${filter_chain}" ${f};
	else
	    filter_file -u "$user" -g "$group" -P "$permissions" -f "${filter_chain}" ${f} ${dir}/${fn};
	fi;
	if [ $? -ne 0 ]; then
	    return 1;
	fi;
    done;

    return 0;
}


#
# A filter that replaces variables of the form '${name}'. The values for these variables
# are taken from the environment and the command line arguments. 
# This filter reads data from the stdin and produces it on stdout.
# Files with environment variables names can be specified with the -W option, and
# files containing key value pairs can be specified with the -D option.
# A replace style can also be provided, "-s '{{ }}'" would replace variables of the form {{name}}.
# 
# Usage:
#  substitute_filter [-W <file>] [-w whitelist] [-d <key>=<value> ] [-D <file> ] [-s <pre post>] [-x]
# Return
#  0 on success
#  1 on error

function substitute_filter()
{
    local -A key_values;
    local key value;
    local LINE;
    local pre="\${"
    local post="}";
    local xml=false;

    OPTIND=1;
    while getopts "W:w:D:d:s:xE" opt; do
	case $opt in
	    w) 
		for key in ${OPTARG//,/ }; do
		    value="${!key}";
		    key_values["$key"]="$value";
		done;;
	    W)
		while read key; do
		    value="${!key}";
		    key_values["$key"]="$value";
		done <<<"$(<$OPTARG)";;
	    d) 
		read key value <<<"${OPTARG/[:=]/ }";
		key_values["$key"]="$value";;
	    D) 
		while read LINE; do
		    read key value <<<"${LINE/[:=]/ }";
		    key_values["$key"]="$value";
		done <<<"$(<$OPTARG)";;
	    E) 
		while read LINE; do
		    read key value <<<"${LINE/[:=]/ }";
		    key_values["$key"]="$value";
		done <<<"$(printenv)";;
	    s)
		read pre post <<<"${OPTARG}";;
	    x)
		xml=true;;
	    *)  true;;
	esac;
    done;
    
    shift $((OPTIND-1));

    if $xml; then
	log -e "XML substitution not supported";
	return 1;
    fi;

    local -a expressions;
    for key in ${!key_values[@]}; do
	value=${key_values[${key}]//\//\\\/};
	expressions+=(-e);
	expressions+=("s/${pre}${key}${post}/${value}/g");
    done;    
    
    if [ ${#expressions[@]} -eq 0 ]; then 
	cat;
	return 0;
    fi;

    
    sed "${expressions[@]}";
    return 0;
}

#
# Copy a directory tree and optionally filter each copied file.
# Symbolic links are copied. If a file is copied, then the modification
# time of the containing directory is modified.
# The source and target must either be a pair of files or a pair of directories.
# or sympolic links.
#
# Usage:
#  copy_tree [-s <source>] [-t <target>] ....
# Return:
#  0 if all files were copied
#  1 if an error occurred
# 
function copy_tree()
{
    local source target;
    OPTIND=1;
    while getopts "s:t:" opt; do
	case "$opt" in 
	    s) source="$OPTARG";;
	    t) target="$OPTARG";;
	esac;
    done;

    shift $((OPTIND-1));

    local filter;
    if [ $# -eq 0 ]; then
	filter=cat;
    else
	filter="$1";
	shift;
    fi;

    if [ -n "$target" ] && ! make_dirs "$target"; then
	log -e "Failed to create target directory: $target";
	return 1;
    fi;
	

#    log -i "$source -> $target : $filter $@";

    if [ -z "$target" ]; then
	if [ -z "$source" ]; then
	    "$filter" "$@";
	    return 0;
	elif [ -f "$source" ]; then
	    "$filter" "$@" < "$source";
	    return 0;
	else
	    log -e "Source not a file : $source";
	    return 1;
	fi;
    fi;

    if [ -z "$source" ]; then
	if [ ! -e "$target" ] || [ -f "$target" ]; then
	    "$filter" "$@" > "$target";
	    return 0;
	fi;
	log -e "Target is not a file file : $source";
	return 1;
    fi;


    if [ -h "$source" ] && [ ! -e "$target" ]; then
	if ! make_dirs "$target"; then
	    log -e "Failed to create target directory for $target";
	    return 1;
	fi;
	ln -s $(readlink "$source") "$target";
	return 0;
    fi;

    if [ -f "$source" ]; then
	if [ ! -e "$target" ];then
	    if ! make_dirs "$target"; then
		log -e "Failed to create target directory for $target";
		return 1;
	    fi;
	elif [ ! -f "$target" ]; then
	    log -e "Target is not a file : $target";
	    return 1;
	fi;
	
	"$filter" "$@" < "$source" > "$target";
	return 0;
    fi;


    if [ ! -d "$source" ]; then
	log -e "Source must be a directory $source";
	return 1;
    fi;

    if [ ! -e "$target" ]; then
	if ! make_dirs "$target"; then
	    log -e "Failed to create target directory for $target";
	    return 1;
	fi;
    elif [ ! -d "$target" ]; then
	log -e "Target is not a directory : $target";
	return 1;
    fi;
    
    for f in $(ls -a "$source"); do
	if [ "$f" != "." ] && [ "$f" != ".." ]; then
	    $(copy_tree -s "$source/$f" -t "$target/$f" "$filter" "$@");
	fi;
    done;
    return 0;
}

#
# Touch multiple files and make their directories if they dont exist.
# Usage
#  ftouch <files>...
# Return 
#  0 on success
function ftouch()
{
    make_dirs "$@";
    touch "$@";
}

#
# Split a search path into its components.
# The search path is is of the form path1:path2:...:pathN
# Usage:
#  split_path path
# Return
#  0 on success and the list of patch components on stdout
#  1 on error
function split_path()
{
    if [ $# -eq 0 ]; then
	log -e "No path specified";
	return 1;
    fi;
    tr ':' ' ' <<<"${1}";
}
