#!/bin/bash

#
# A simple message that goes to stderr.
# 
function message()
{
    local opts=

    OPTIND=1;
    while getopts "neE" opt; do
	case "$opt" in
	    n) opts="$opts -n";;
	    e) opts="$opts -e";;
	    E) opts="$opts -E";;
	esac;
    done;
    shift $((OPTIND-1));
    
    echo $opts "$@" 1>&2;
    return 0;
}


#
# Logging must not make use for globals package!!!.
# The log level is either informational (-i), a warning
# (-w), an error (-e), or a debug message (-d). 
# The stdin can be logged by specifing the redirect option -r.
# 
#
# Log an informational message.
# Usage:
#  log [-i] [-e] [-w] [-d] [-r] [<message...>]
# Return:
#  0 always
#
function log() {
    local level="INFO";
    local color="${FG_GREEN}";
    local colorize=true;
    local time=true;
    local use_stdin=false;

# only one option supported right now; so use an if-statement
    
    local separator=${LOG_SEPARATOR:-|};

    OPTIND=1;
    while getopts "eiwctdr" opt; do
	case $opt in
	    e) level="ERROR"; color="${FG_RED}";;
	    i) level="INFO"; color="${FG_GREEN}";;
	    d) level="DEBUG"; color="${FG_BLUE}";;
	    w) level="WARNING"; color="${FG_YELLOW}";;
	    c) colorize=false;;
	    t) time=false;;
	    r) use_stdin=true;;
	esac;
    done;
    shift $((OPTIND-1))

    if ! $use_stdin && [ -z "$*" ]; then
	return 0;
    fi;

    if [ "$level" == "DEBUG" ] && [ "$LOG_LEVEL" != "DEBUG" ]; then
	# consume stdin if necessary
	$use_stdin && cat > /dev/null;
	return 0;
    fi;

    if $colorize; then
	level="${color}${level}${FG_COLOR}";
    fi;
    if $time; then
	message -n "$(now)${separator}";
    fi;
    
    message -n "${level}${separator}${$}${separator}";
    if $use_stdin; then
	cat 1>&2;
    else
	message -- "$*";
    fi;
    return 0;
}

#
# Log a warning if the first argument is non-zero.
# Usage:
#  warn <code> <message>
# Return
#  <code>
#
function warn()
{
    local code=$1;
    shift;

    if [ $code -ne 0 ]; then
	log -w "$@";
    fi;

    return $code;
}


#
# Log an error if the first argument is non-zero.
# Usage:
#  error <code> <message>
# Return
#  <code>
#
function error()
{
    local code=$1;
    shift;

    if [ $code -ne 0 ]; then
	log -e "$@";
    fi;

    return $code;
}
