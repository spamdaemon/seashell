#!/bin/bash
#
# Time related functions. This file is required by logging.sh
# and must thus not contain any logging code.
#

#
# Get a dateformat
#
# Usage:
#  get_dt_format [-t] [-s]
#   -s : get short version of format
#   -t : emit timezone information 
# 
# Return:
#  0 and the format string on stdout
#
function get_iso8601_format()
{
    local showTz=0;
    local dtFormat="%Y-%m-%dT%H:%M:%S";
    local tzFormat="%:z";

      OPTIND=1;
   while getopts "ts" opt; do
	case $opt in 
	    t) showTz=1;;
	    s) dtFormat="%Y%m%dT%H%M%S";
		tzFormat="%z";;
	esac;
    done;
    shift $(($OPTIND-1))

    if [ $showTz -eq 1 ]; then
	echo "${dtFormat}${tzFormat}";
    else
	echo "${dtFormat}";
    fi;
}

#
# Get the current date and time in the variant of ISO8601.
# 
# Usage:
#  now [-u] [-t] [-s]
#   -u : emit UTC time
#   -s : emit short version without dashes and colons
#   -t : emit timezone information 
# Return:
#  0 and the date on stdout
#
function now()
{
    local tz=;
    local showTz=;
    local short=;

    OPTIND=1;
    while getopts "uts" opt; do
	case $opt in 
	    u) tz='--utc';;
	    t) showTz='-t';;
	    s) short='-s';;
	esac;
    done;
    shift $(($OPTIND-1))

    date $tz "+$(get_iso8601_format $short $showTz)"

    return $?;
}

