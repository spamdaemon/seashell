globals_init;
if [ $? -ne 0 ]; then
    exit 1;
fi;

cleanup_init;
if [ $? -ne 0 ]; then
    globals_destroy;
    exit 1;
fi;

make_init;
if [ $? -ne 0 ]; then
    exit 1;
fi;
