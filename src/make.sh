#!/bin/bash
#
# This file contains make-related functionality.
# 
# 

# these are package private variables
# and should not be used outside this package

get_target_file()
{
    echo "build/$1";
}


define_target()
{
    local target;
    local dependsOn;
    local ingredients;
    local buildFN;
    local opt;

    OPTIND=1;
    while getopts "t:d:i:f:" opt; do
	case $opt in 
	    t) target="${OPTARG}";;
	    d) dependsOn="${OPTARG}";;
	    i) ingredients="${OPTARG}";;
	    f) buildFN="${OPTARG}";;
	    *) log -e "Invalid option $opt"; exit 1;;
	esac;
    done;
    

    shift $(($OPTIND-1))

    local targetFile=$(get_target_file "$target");
    

    local procedure="local needRebuild=0;";

    # rebuild all targets need to be rebuilt
    procedure+="if build_targets ${dependsOn}; then ";
    procedure+=" if [ -f '$targetFile' ]; then ";
    procedure+="  if compare_mod_time -c gt '${targetFile}' ${ingredients} 1>/dev/null 2>&1; then ";
    procedure+="   return 0;"
    procedure+="  fi;"
    procedure+=" fi;"
    procedure+="fi;"

    if [ -z "${buildFN}" ]; then
	buildFN="build_target_$target";
	eval "function ${buildFN}() { $(tee) ; }"
    fi;
    
    procedure+="mkdir -p '$(dirname $targetFile)';";
    procedure+="${buildFN} $ingredients > ${targetFile};";
    procedure+="if [ \$? -ne 0 ]; then rm -f ${targetFile}; echo 'Error: $target'; exit 1; fi;";
    procedure+="touch '$targetFile';";
    procedure+="return 1;";

    ALL_TARGETS["${target}"]="$procedure";
}

exec_build_procedure()
{
    eval "$1";
}


build_target()
{
    if [ -n "${TARGET_UP_TO_DATE[$1]}" ]; then
	return ${TARGET_UP_TO_DATE["$1"]};
    fi;

    if [ -z "${ALL_TARGETS[$1]}" ]; then
	log -e "No such target $1";
	return 2;
    fi;

    local proc="${ALL_TARGETS[$1]}"
    exec_build_procedure "${ALL_TARGETS[$1]}";
    if [ $? -eq 0 ]; then
	log -i "Target <$1> is up-to-date";
	TARGET_UP_TO_DATE["$1"]=0;
	return 0;
    else
	log -w "Target <$1> was out-of-date";
	TARGET_UP_TO_DATE["$1"]=1;
	return 1;
    fi;
}


build_targets()
{
    local t;
    for t in "$@"; do
	if [ -z "${ALL_TARGETS[$t]}" ]; then
	    log -e "No such target $t";
	    return 1;
	fi;
    done;

    # build as many targets possible
    # if all none needed rebuilding, then
    # eventually, we'll return 0
    while [ $# -ne 0 ]; do
    	build_target "$1";
	if [ $? -ne 0 ]; then
	    break;
	fi;
	shift;
    done;

    # all targets have been rebuilt
    if [ $# -eq 0 ]; then
	return 0;
    fi;

    shift;
    while [ $# -ne 0 ]; do
    	build_target "$1";
	if [ $? -ne 0 ]; then
	    break;
	fi;
	shift;
    done;

    return 1;
}

define_new_target()
{
    local TARGET FN DEPENDS NEEDS;

    local LINE="${2// /\ }";
    log -i "Target def: " "$LINE";
    read TARGET DEPENDS NEEDS FN  <<<"$LINE";


    if [ -z "$FN" ]; then
	FN=true;
    fi;

    if [ "$DEPENDS" == "-" ]; then
	DEPENDS=
    else
	DEPENDS="${DEPENDS//|/ }";
    fi;
    if [ "$NEEDS" == "-" ]; then
	NEEDS=
    else
	NEEDS="${NEEDS//|/ }";
    fi;
    define_target -t "$TARGET" -f "$FN" -d "${DEPENDS}" -i "${NEEDS}" ;
}

# 
# Make a list of targets defined in a build table.
# 
# Usage:
#  make_targets <target>...
# 
function make_targets()
{

    ALL_TARGETS=
    TARGETS_UP_TO_DATE=

    readarray -t -c 1 -C define_new_target;

    build_targets "$@";

    ALL_TARGETS=
    TARGETS_UP_TO_DATE=

    return $?;
}

make_init()
{
    declare -A ALL_TARGETS;
    declare -A TARGET_UP_TO_DATE;
    return 0;
}

