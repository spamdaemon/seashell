
#
# Mount an iso image under the specified name.
# If no mount-point is provided, then a temporary one will be created.
# The -u option can be specified to automatically unmount the iso on exit.
# To the 
# Usage:
#  mount_iso [-u] [-m <mount_point>] [-t <tmpfolder> <iso>
# Return:
#  0 if the iso image was mounted and the mount handle on stdout.
#  1 if the iso image was not mounted.
#
function mount_iso()
{
    local MOUNT=;
    local TEMP_MOUNT=;
    local UNMOUNT_ON_EXIT=false;
    local UNMOUNT_EXPRESSION=0;
    local IS_TEMP_MOUNT=false;
    local TEMP_DIR=/tmp;

    OPTIND=1;
    while getopts "m:ut:" opt; do
	case $opt in
	    m) MOUNT="$OPTARG";;
	    t) TMP_DIR="$OPTARG";;
	    u) UNMOUNT_ON_EXIT=true;;
	esac;
    done;

    shift $((OPTIND-1));

    local ISO="$1";

    if [ -z "$ISO" ] || [ ! -f "$ISO" ]; then
	log -e "No iso image to mount";
	return 1;
    fi;

    if [ -z "$MOUNT" ]; then
	mkdir -p "${TEMP_DIR}";
	MOUNT=$(mktemp -d -p "${TEMP_DIR}" iso.XXXXXX);
	TEMP_MOUNT="$MOUNT";
	IS_TEMP_MOUNT=true;
    fi;

    mkdir -p "$MOUNT";
    if [ ! -d "$MOUNT" ]; then
	log -e "Failed to get mount point $MOUNT";
	rm -rf "$TEMP_MOUNT";
	return 1;
    fi;

    ISO_HANDLE=$(to_handle "$MOUNT $IS_TEMP_MOUNT $ISO")
    if $UNMOUNT_ON_EXIT; then
	local trapID="mount_iso${MOUNT}${IS_TEMP_MOUNT}${ISO}";
	add_exit_trap "${trapID}" "echo UMOUNT && umount_iso '$ISO_HANDLE'";
    fi;

    mount -o loop,ro "$ISO" "$MOUNT";
    if [ $? -ne 0 ]; then
	log -e "Failed to mount $1 as $MOUNT";
	rm -rf "$TMP_MOUNT";
	return 1;
    fi;
    echo "$ISO_HANDLE";

    log -i "Mounted $ISO on $MOUNT";
    return 0;
}

#
# Unmount one or more iso images.
# Usage:
#  umount_iso <mount_handle>...
#
function umount_iso()
{
    if [ -z "$1" ]; then
	log -e "No iso to unmount";
	return 1;
    fi;
     
    local RETVAL=0;
    while [ -n "$1" ]; do
	local RAW=$(from_handle "$1");
	local MOUNT IS_TEMP_MOUNT ISO;
	read MOUNT IS_TEMP_MOUNT ISO <<<"$RAW";
	
	if [ -z "$ISO" ]; then
	    log -e "Invalid iso handle $1";
	    RETVAL=1;
	else
	    local trapID="mount_iso${MOUNT}${IS_TEMP_MOUNT}${ISO}"
	    
	    remove_exit_trap "$trapID";
	    
	    umount "$MOUNT";
	    if [ $? -ne 0 ]; then
		log -e "Failed to unmount $OPTARG";
		RETVAL=1;
	    fi;
	    
	    if $IS_TEMP_MOUNT; then
		rm -rf "$MOUNT";
	    fi;
	    
	    log -i "Unmounted ${ISO} from ${MOUNT}";
	fi;
	shift;
    done;

    return $RETVAL;
}

# Get the mount point from a handle 
#
# Usage:
#  mount_point <handle>
# Return:
#  0 and the directory for the mount point
#  1 on error
#
function mount_point()
{
    local MOUNT IS_TEMP_MOUNT ISO;

    if [ -z "$1" ]; then
	log -e "No iso to unmount";
	return 1;
    fi;

    local RAW=$(from_handle "$1");
    read MOUNT IS_TEMP_MOUNT ISO <<<"$RAW";
    if [ -z "$ISO" ]; then
	log -e "Invalid iso handle $RAW";
	return 1;
    fi;

    echo "$MOUNT";
    return 0;
}
