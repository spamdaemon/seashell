

#
# Given a distribution directory, guess the distribution and version.
# 
# Usage:
#  detect_distribution <distribution>
# Return:
#  0 if the distribution and version were detected and written to stdout.
#  1 if the distribution could not be detected
# 
function detect_distribution()
{
    local dist="$1";
    if [ -z "$dist" ]; then
	log -e "Missing distribution directory";
	return 1;
    fi;

    local distribution=;
    local version=;
    local baseurl=;
    local arch=;
    
    if [ -d "$dist/suse" ] && [ -f "$dist/content" ]; then
	local distro_version=$(sed -n -re 's@^DISTRO\s+(.+:){3}+([^:]+):([0-9.]+).+$@\2 \3@p' $dist/content);
	read distribution version <<<"$distro_version";
	baseurl="dir:$dist/suse/\$basearch";
	arch=x86_64;
    elif [ -f "$dist/.treeinfo" ] && [ -f "${dist}/repodata/repomd.xml" ] && grep -Po '^name\s*=\s*Fedora' $dist/.treeinfo 1>/dev/null 2>&1; then 
	distribution=fedora;
	version=$(sed -n -re 's@^version\s*=\s*(.+)$@\1@p' $dist/.treeinfo);
	baseurl="file:$dist";
	arch=x86_64;
    elif [ -f "$dist/.treeinfo" ] && [ -f "${dist}/repodata/repomd.xml" ] && grep -Po '^family\s*=\s*Red Hat Enterprise Linux' $dist/.treeinfo 1>/dev/null 2>&1; then 
	distribution=rhel;
	version=$(sed -n -re 's@^version\s*=\s*(.+)$@\1@p' $dist/.treeinfo);
	baseurl="file:$dist";
	arch=x86_64;
    elif [ -d "$dist/ubuntu" ] && [ -f "$dist/dists/stable/Release" ]; then
	distribution=ubuntu;
	version=$(sed -n -re 's@^Version\s*:\s+(.+)$@\1@p' $dist/dists/stable/Release);
	arch=x86_64;
    fi;


    if [ -z "$distribution" ] || [ -z "$version" ]; then
	log -e "Failed to detect distribution for $dist";
	return 1;
    fi;

    echo $distribution $version $arch $baseurl;
    return 0;
}

# 
# The packagers to use for a given distribution.
# Distribution versions are separated by a dash
declare -A PACKAGERS;
PACKAGERS["opensuse"]="zypper";
PACKAGERS["fedora"]="yum";
PACKAGERS["rhel"]="yum";
PACKAGERS["ubuntu"]="apt";

#
# Default packages for a distribution
#
declare -A DEFAULT_PACKAGES;
DEFAULT_PACKAGES["opensuse"]="openSUSE-release kernel-default dracut iputils nfs-client";
DEFAULT_PACKAGES["fedora"]="fedora-release kernel dracut";
DEFAULT_PACKAGES["rhel"]="redhat-lsb-core kernel dracut";


#
# Get the packager for the specified distribution.
#
# Usage:
#  get_packager <distribution> <version>
# Return
#  the name of the packager for the distribution and the name of the executable to use
# 
function get_packager()
{
    local entry="${PACKAGERS[${1}-${2}]}";
    if [ -z "$entry" ]; then
	entry="${PACKAGERS[${1}]}";
    fi;

    if [ -z "$entry" ]; then
	log -e "Failed to determine the package manager for $1 $2";
	return 1;
    fi;
    
    echo $entry;
    return 0;
}

#
# Get the packager for the specified distribution.
#
# Usage:
#  get_packager <distribution> <version>
# Return
#  the name of the packager for the distribution and the name of the executable to use
# 
function get_default_packages()
{
    local entry="${DEFAULT_PACKAGES[${1}-${2}]}";
    if [ -z "$entry" ]; then
	entry="${DEFAULT_PACKAGES[${1}]}";
    fi;

    if [ -z "$entry" ]; then
	log -e "Failed to determine the default packages for $1 $2";
	return 1;
    fi;
    
    echo $entry;
    return 0;
}




# 
# Get the linux distribution on the current host or the specified
# root file system.
# Usage
#  get_distro [-r root]
# Return:
#  0 and the distribution and version (separated by tab)
#  1 if the distribution or version could not be determined
# 
function get_distro()
{
    local root=/;

    while getopts "r:" opt; do
	case $opt in
	    r) root="$OPTARG";;
	esac;
    done;

    local distro=$(grep -P "^NAME=" ${ROOT}/etc/os-release | cut -d"=" -f 2);
    if [ $? -ne 0 ]; then
	log -e "Failed to determine distribution";
	return 1;
    fi;

    local version=$(grep -P "^VERSION_ID=" ${ROOT}/etc/os-release | cut -d"=" -f 2);
    if [ $? -ne 0 ]; then
	log -e "Failed to determine $distro version";
	return 1;
    fi;
    # get rid of quotes around the version
    version=$(eval echo $version);

    echo -e "$distro\t$version";
    return 0;
}

rpm_install()
{
   local root=/;

   OPTIND=1;
   while getopts "r:" opt; do
       case $opt in
	   r) root="$OPTARG";;
       esac;
   done;
   shift $((OPTIND-1));
    
   rpm --root "$root" --upgrade "$@";

   true;
}


rpm_init_db()
{
    local dbpath="$1/var/lib/rpm";
    rm -rf "$dbpath";
    mkdir -p "$dbpath";
    log -i "Initializing rpm database $dbpath";
    rpm --initdb --dbpath "$dbpath";
}


#
# Initialize the package manager using the current 
# distribution's package manager.
#
install_package_manager()
{
    local root=;

    OPTIND=1;
    while getopts "r:" opt; do
	case $opt in
	    r) root="$OPTARG";;
	esac;
    done;
    shift $((OPTIND-1));

    if [ -z "$root" ]; then
	log -e "No root specified";
	return 1;
    fi;

    rpm_init_db "$root";
    mkdir -p "$root";
    return 0;
}

#
# Install packages from an ISO CD. The installation
# is performed into the specified root directory.
# Multiple iso's can be specified at the same time.
#
# This function requires root priviledges in order to 
# mount the ISO packages and install root packages.

# Usage:
#  install_packages [-i <iso>] -r <root> <package>...
# Return:
#  0 on sucess
#  1 on error
function install_packages()
{
    local isos=;
    local root=;
    local iso;

    OPTIND=1;
    while getopts "i:r:" opt; do
	case $opt in
	    i) 
		iso="$(readlink -e "$OPTARG")";
		isos+=" $(mount_iso -u "$iso" )";
		if [ $? -ne 0 ]; then
		    log -e "Failed to mount iso $iso";
		    umount_iso ${isos};
		    return 1;
		fi;;
	    r) 
		root=$(readlink -f "$OPTARG");;
	esac;
    done;
    shift $((OPTIND-1));

    if [ -z "$isos" ]; then
	log -e "No iso images specified";
	return 1;
    fi;
    
    if [ -z "$root" ]; then
	log -e "No installation root specified";
	umount_iso ${isos};
	return 1;
    fi;
    
    local distro version url mount_point;
    
    for iso in ${isos}; do
	mount_point="$(mount_point ${iso})";
	read distro version arch url <<<"$(detect_distribution "$mount_point")";
	if [ $? -eq 0 ]; then
	    log -i "Detected distribution : $distro $version for $mount_point";
	    break;
	fi;
	distro=
	version=
	arch=
	url=
    done;

    if [ -z "$distro" ]; then
	log -e "Failed to detect distribution";
	umount_iso ${isos};
	return 1;
    fi;
    
    local packager="$(get_packager "$distro" "$version")";
    if [ -z "$packager" ]; then
	umount_iso ${isos};
	return 1;
    fi;
    local default_packages=$(get_default_packages  "$distro" "$version");
    
    if [ ! -d "$root" ]; then
	install_package_manager -r "$root";
	if [ $? -ne 0 ]; then
	    umount_iso ${isos};
	    return 1;
	fi;
    fi;
    
    local RETVAL=0;
    local repos=;
    local repo;

    for iso in ${isos}; do
	log -i "${packager}_create_repository -r "$root" -d $(mount_point "${iso}")";
	repo=$(${packager}_create_repository -r "$root" -d "$(mount_point "${iso}")");
	if [ $? -ne 0 ]; then
	    RETVAL=1;
	    break;
	fi;

	repos+=" $repo";
	log -i "Repo : $repo";
	cat "$repo" | log -r -i;
    done;
    
    if [ $RETVAL -eq 0 ]; then
	if [ $# -ne 0 ]; then
	    ${packager}_install_packages -r "$root" "$@";
	elif [ -n "$default_packages" ]; then
	    ${packager}_install_packages -r "$root" $default_packages;
	else
	    log -w "No packages to install";
	fi;
    fi;

    if [ $? -ne 0 ]; then
	RETVAL=1;
    fi;
    
    for repo in $repos; do
	rm -rf $repo;
    done;
    
    umount_iso ${isos};
    if [ $? -ne 0 ]; then
	RETVAL=1;
    fi;

    return $RETVAL;
}

#
# Build an initrd for booting over the network.
# 
#
function build_initrd_nfs()
{
  local root;
  local kernel;
  local image=initrd_nfs;

  OPTIND=1;
  while getopts "r:k:" opt; do
      case "$opt" in
	  r) root=$(readlink -f "$OPTARG");;
	  k) kernel="$OPTARG";;
	  o) image="$OPTARG";;
      esac;
  done;
  shift $((OPTIND-1));

  if [ ! -d "$root" ]; then
      log -e "No such root directory $root";
      return 1;
  fi;

  if [ -z "$image" ]; then
      log -e "Invalid image";
      return 1;
  fi;

  if [ ! -f "${root}/boot/vmlinuz-${kernel}" ]; then
      log -e "No such kernel $kernel";
      return 1;
  fi;

  rm -f "${root}/boot/${image}";

  chroot "${root}" dracut  --filesystems "nfs" -m "nfs network base" "/boot/${image}.gz" ${kernel} 1>&2 | log -i;

  echo -en "${root}/boot/${image}.gz\0";
  echo -en "${root}/boot/vmlinuz-${kernel}\0";
}
