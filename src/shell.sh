#!/bin/bash

#
# Determine if the argument is a positive integer
# consisting of only digits.
# 
# Usage
#  is_integer <value>
# Return
#  0 if value is an integer
#  1 false otherwise
#
function is_decimal_integer()
{
    local number;
    number=$(grep -Po '^([\d]+)$' <<<"$1" 2>/dev/null);
    return $?;
}

#
# Reverse a list
# Usage
#  reverse <arg1> ... <argN>
# Return
#  0 and <argN> ... <arg1> on stdout
function reverse_list()
{
    local res=;
    local x=;
    for x in "$@"; do
	if [ -z "$res" ]; then
	    res="${x}";
	else
	    res="${res} ${x}";
	fi;
    done;
    echo "$res";
    return 0;
}


#
# Get the last of the arguments. 
# Usage
#  last_arg <arg1> ... <argN>
# Return
#  print argN on stdout and return 0
#  1 if there are no arguments
function first_arg()
{
    if [ $# -eq 0 ]; then
	return 1;
    fi;
    echo $1;
    return 0;
}


#
# Get the last of the arguments. 
# Usage
#  last_arg <arg1> ... <argN>
# Return
#  print argN on stdout and return 0
#  1 if there are no arguments
function last_arg()
{
    if [ $# -eq 0 ]; then
	return 1;
    fi;
    echo "${@: -1:1}"
    return 0;
}



# 
# Get all, but the last argument
# Usage
#  first_args <arg1>...<argN>
# Return
#  print <arg1>...<argN-1> on stdout and return 0
#
function first_args()
{
    if [ $# -eq 0 ]; then
	return 1;
    fi;
    local n=$(($#-1));
    echo "${@: 1:$n}";
    return 0;
}


# 
# Get all, but the last argument
# Usage
#  first_args <arg1>...<argN>
# Return
#  print <arg1>...<argN-1> on stdout and return 0
#
function last_args()
{
    if [ $# -eq 0 ]; then
	return 1;
    fi;
    shift;
    echo "$@";
    return 0;
}

#
# Open a file descriptor for a file or stdin.
# Usage
#  open_fd <mode> <fd> [<file>] 
# Return:
#  0 if the file descriptor was opened
#  1 if the file descriptor was not open
#
open_fd()
{
    local text;

    if [ "$1" == "r" ]; then
	if [ -n "$3" ]; then
	    text="$2<$3";
	elif [ -n "$2" ]; then
	    text="$2<&0";
	fi;
    elif [ "$1" == "w" ]; then
	if [ -n "$3" ]; then
	    text="$2>$3";
	elif [ -n "$2" ]; then
	    text="$2>&1";
	fi;
     fi;
    if [ -z "$text" ]; then
	abort -m"open_fd: invalid arguments";
    fi;
 
    eval exec $text;
    return $?;
}

#
# Close file descriptors.
#
# Usage:
#  close_fd <file-descriptor>...
# Return:
#  0 

close_fd()
{
    for f in "$@"; do
	eval exec "$f>&-" ;
	abort_on_error $? "Failed to close file descriptor $f";
    done;
    return 0;
}


