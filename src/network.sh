#!/bin/sh


#
# Convert a network prefix to an IPv4 netmask.
# Usage
#  prefix_to_netmask <prefix>
# Return:
#  0 on success and netmask on stdout
#  1 on error
#
function prefix_to_netmask()
{

    if ! is_decimal_integer $1; then
	return 1;
    fi;
    
    local prefix=$1;

    local -a bits=( [8]=255 [7]=254 [6]=252 [5]=248 [4]=240 [3]=224 [2]=192 [1]=128 [0]=0 );
    local -a mask=( [0]=0 [1]=0 [2]=0 [3]=0 );
    
    for i in 0 1 2 3; do
	if (( $prefix < 1 )); then
	    break;
	elif (( $prefix > 7 )); then
	    mask[$i]=255;
	    (( prefix=$prefix-8));
	else
	    mask[$i]=${bits[$prefix]};
	    prefix=0;
	    break;
	fi;
    done;

    if [[ $prefix != 0 ]]; then
	log -e "Invalid prefix $1";
	return 1;
    fi;
    
    echo "${mask[0]}.${mask[1]}.${mask[2]}.${mask[3]}";
    return 0;
}

#
# Convert an IPv4 netmask into a prefix. 
# Usage
#  netmask_to_prefix <netmask or prefix>
# Return
#  0 on success and the prefix on stdout
#  1 on error
#
function netmask_to_prefix()
{
# this function taken from here:
# http://www.linuxquestions.org/questions/programming-9/bash-cidr-calculator-646701/
    
    local nbits dec
    local -a octets=( [255]=8 [254]=7 [252]=6 [248]=5 [240]=4
        [224]=3 [192]=2 [128]=1 [0]=0 )
	
    while read -rd '.' dec; do
        [[ -z ${octets[dec]} ]] && log -e "$dec is not recognised" && return 1;
        (( nbits += octets[dec] ));
        (( dec < 255 )) && break;
    done <<<"$1."
    
    echo "$nbits"
    return 0;
}

# 
# get_prefix
# 
# Usage:
#  get_prefix <netmask or prefix>
# Return
#  0 and the prefix on stdout
#  1 on error
function get_prefix()
{
    local netmask;
    netmask=$(prefix_to_netmask $prefix 2>/dev/null);

    if [ $? -eq 0 ]; then
	echo $1;
	return 0;
    fi;

    netmask_to_prefix $1;
}

# 
# get_net_mask
# 
# Usage:
#  get_netmask <netmask or prefix>
# Return
#  0 and the netmask on stdout
#  1 on error
function get_netmask()
{
    local prefix;
    prefix=$(netmask_to_prefix $1 2>/dev/null);
    if [ $? -eq 0 ]; then
       echo $1;
       return 0;
    fi;
    
    prefix_to_netmask $1;
}


#
# Determine the network from an ipaddress and a netmask or prefix
# Usage
#  network_address <netmask_or_prefix> <ip4address>
# Return
#  0 on success and the ipv4 network address
#  1 on error
#
function get_network()
{
   local netmask;
   netmask=$(get_netmask $1);
   if [ $? -ne 0 ]; then
       return 1;
   fi;
   local -a addr;
   local -a mask;

   IFS=. read -a addr  <<<"$2";
   IFS=. read -a mask <<<"$netmask";

   for i in 0 1 2 3; do
       (( addr[$i]=${addr[$i]}&${mask[$i]} ));
   done;

   echo "${addr[0]}.${addr[1]}.${addr[2]}.${addr[3]}";
   return 0;
}

#
# Given a network and prefix or netmask, determine the broadcast address.
#  
# Usage:
#  broadcast_address [netmask] <cidr or ipaddress> .
# Return:
#  0 and the the broadcast address on stdout
#  1 on error
get_broadcast_address()
{
   local netmask=$(get_netmask $1);
   local network=$(get_network $1 $2);

   local -a addr;
   local -a mask;
   IFS=. read -a addr  <<<"$network";
   IFS=. read -a mask <<<"$netmask";

   local -a bcast;
   for i in 0 1 2 3; do
       (( bcast[$i]=(~${mask[$i]}&255)|${addr[$i]} ))
   done;
   echo "${bcast[0]}.${bcast[1]}.${bcast[2]}.${bcast[3]}";
   
   return 0;
}

#
# Determine the cidr network an ipaddress and a netmask or prefix.
# Usage
#  cidr_address <netmask_or_prefix> <ip4address>
# Return
#  0 on success and the cidr address
#  1 on error
#
function get_cidr_address()
{
    
    if [ $# -eq 1 ]; then
# fixme: verify that it's a proper address
	echo $1;
	return 0;
    fi;

    if [ $# -ne 2 ]; then
	log -e "Too many arguments $@";
	return 1;
    fi;

    local prefix;
    prefix=$(get_prefix $1);
    if [ $? -ne 0 ]; then
	return 1;
    fi;

    echo "${2}/${prefix}";
    return 0;
}

#
# Split a cidr address into address, network, and netmask
# Usage:
#  split_cidr_address <address>/<prefix>
# Return:
#  0 on success and a line of output <address> <network> <netmask>
#  1 on error
# 
function split_cidr_address()
{
    local addr=$(sed -re 's|^(.+)/.*$|\1|g' <<<"$1")
    local prefix=$(sed -re 's|^.+/(.+)|\1|g' <<<"$1")

    local netmask=$(get_netmask $prefix);
    local network=$(get_network $netmask $addr);

    echo "$addr $network $netmask"
}
