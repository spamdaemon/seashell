#!/bin/bash


#
# Read and normalize a file that is a table.
# This function reads on line at a time, and removes all 
# any text after a '#' symbol and deletes complete empty lines
# or lines only containing whitespace.
# 
# Usage:
#  table [-d <delim>] [-s] [-c <cols>] [<file>]
#   -s sequece multiple delimters into a single one
#   -s cols the columns to output
# Return:
#  0 on success
#  1 on failure
# Input:
#  if no file is specified, then reads a table from stdin
# Output:
#  produces a normalized version of the table on stdout
#
function table() {

    local delim='\t';
    local squeeze;
    local columns;

    OPTIND=1;
    while getopts "sd:c:" opt; do
	case $opt in 
	    d) delim="$OPTARG";;
	    s) squeeze="y";;
	    c) columns="$OPTARG";;
	esac;
    done;
    shift $(($OPTIND-1));

    if [ -n "$squeeze" ]; then
	squeeze="tr -s '$delim'";
    else
	squeeze="cat";
    fi;

    local awk_expr;
    if [ -n "$columns" ]; then
	local awk_expr=;
	for c in $columns; do
	    if [ -n "$awk_expr" ]; then
		awk_expr="${awk_expr},";
	    fi;
	    awk_expr="${awk_expr}\$$c";
	done;
    else
	awk_expr='$0';
    fi;

    # a local function
    fn() {
	sed -re 's/\s*(#.*){0,1}$//g' | grep -v '^$' | $squeeze | awk -v "OFS=$delim" -F "$delim" "{ print $awk_expr }"
    }
    
    if [ $# -eq 0 ]; then
	fn;
    else
	fn < "$1";
    fi
}

