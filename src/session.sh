
#
# Get the session associated with the specified process
# If the process id is not specified it retrieves the session
# if the current process
#
# Usage:
#  get_session <pid>
# Return
#  the session id of the process on stdout
function get_session()
{
    local pid=$$;
    if [ $# -eq 1 ]; then
	pid="$1";
    fi;
    local session="$(ps -o sess= "$pid" 2>/dev/null)";
    if [ -n "$session" ]; then
	echo $session; # do not echo in quotes!
	return 0;
    else
	return 1;
    fi;
}

#
# Get a list of processes in the specified session.
# If a session id is not specified, then the current session
# is used.
#
# Usage:
#  get_session_members <sid>
# Return the member of the session at the time of the call
# 
function get_session_members()
{
    local members;
    local sid="$1";
    if [ $# -eq 0 ]; then
	sid="$(get_session)";
    fi;
    members="$(ps -o pid= -s "$sid")";
    if [ -n "$members" ]; then
	echo $members; # do not echo in quotes!
	return 0;
    else
	return 1;
    fi;
}

#
# Start a program in a new session and place it in the background.
#
# Usage:
#  start_session <program> [<args>...]
# Return
#  0 and the process that was started on stdout
function start_session()
{
    local pid cur new;
    
    cur="$(get_session)";
    # create new session in the background
    setsid "$@" &
    pid=$!
    
    # wait until session id o the process is not that of the current
    # process, because there is a slight chance that this process
    # will continue before the new session is established
    new="$(get_session "$pid")"
    # use busy wait
    while [ "$new" == "$cur" ]; do
	new="$(get_session "$pid")";
    done;
    if [ -n "$new" ]; then
	echo "$new";
	return 0;
    else
	return 1;
    fi;
}

#
# Stop a session by sending the TERM signal to all processes in a session. 

# Usage:
#  stop_session sid
# Return 1 if the session does not exist,
#
function stop_session()
{
    if [ $# -eq 0 ]; then
	return 1; # session must be specified
    fi;
    local members="$(get_session_members "$1")";
    kill -TERM -- $members 2>/dev/null;
    return 0;
}

#
# Kill a session by sending the KILL signal to every member 
#
# Usage:
#  kill_session sid
# Return 1 if the session does not exist,
#
function kill_session()
{
    if [ $# -eq 0 ]; then
	return 1; # session must be specified
    fi;
    local members="$(get_session_members "$1")";
    while [ -n "$members" ]; then
	kill -KILL -- $members 2>/dev/null;
	members="$(get_session_members "$1")";
    fi;
    return 0;
}
