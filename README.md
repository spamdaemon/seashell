seashell
========

A bash libary for system integration and administration tasks.


Motiviation
===========

I needed to build a Linux setup that was PXE bootable and onto multiple
different hosts. I wrote a lot of scripts that were pretty ad-hoc and so we're
not easily reusable. So, I decided to bring everything that I had done into a
single library that I could reuse on future projects.


