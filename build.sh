#!/bin/bash

SCRIPT="$(readlink -e "$0")";
SCRIPT_DIR="$(dirname "$SCRIPT")";
cd "$SCRIPT_DIR";

rm -rf .build;
mkdir -p .build/bin .build/lib;

LIB=.build/lib/seashell.bash;
SRC=`find src -type f -a -name \*.sh | sort`;

cat $SRC > ${LIB};

EXPORTS=`sed -nre 's/^function\s+([^(]+)\s*\(\).*$/\1/p' ${LIB}`;

for f in $EXPORTS; do
    cat > .build/bin/$f <<EOF
#!/bin/bash

SCRIPT="\$(readlink -e "\$0")";
SCRIPT_DIR="\$(dirname "\$SCRIPT")";
source \${SCRIPT_DIR}/../lib/seashell.bash;

$f "\$@";
EOF
    chmod ugo+x .build/bin/$f;
done;


./test_runner;
