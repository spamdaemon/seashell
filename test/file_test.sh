#!/bin/sh


expect -s 1 compare_mod_time /etc/hosts /etc/hosts;
expect -s 1 compare_mod_time -c lt /etc/hosts /etc/hosts;
expect -s 1 compare_mod_time -c gt /etc/hosts /etc/hosts;
expect -s 0 compare_mod_time -c eq /etc/hosts /etc/hosts;

expect -m "/foo/bar" abs_path '/foo/bar';
expect -m "/etc/hosts" abs_path '/etc/hosts';

# Test a normal filter of a file
expect filter_file -f "fgrep localhost" -f "fgrep -v jupiter" "/etc/hosts" "/tmp/foo";
expect fgrep "localhost" /tmp/foo;
expect fgrep -v "jupiter" /tmp/foo;
rm -f /tmp/foo;

# test filtering multiple files
expect filter_files -f "fgrep localhost" -f "fgrep -v jupiter" -o /tmp /etc/hosts
expect fgrep "localhost" /tmp/hosts;
expect fgrep -v "jupiter" /tmp/hosts;
rm -f /tmp/hosts;

