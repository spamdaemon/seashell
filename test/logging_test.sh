#!/bin/sh


logIt() {
    result=`log -c "$@" 2>&1`;
    echo $result;
}


expect -m "default log level" -v "[INFO] Hello" logIt Hello;
expect -m "info log level" -v "[INFO] Hello" logIt -i Hello;
expect -m "info warn level" -v "[WARNING] Hello" logIt -w Hello;
expect -m "info error level" -v "[ERROR] Hello" logIt -e Hello;
