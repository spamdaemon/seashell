#!/bin/sh


abortIt()
{
    abort "$@" 2>&1;
}

expect -m "Abort without message" -s 17 abortIt 17;
expect -m "Abort with message" -v "[ERROR] Message" -s 17 -- abortIt -m Message 17;
